# Apertis Packaging CI

Apertis stores the source of all the shipped packages in GitLab and uses a set
of GitLab CI pipelines to manage the workflows to:

* land updated sources to OBS which will then build the binary outputs
* pull updates from upstream distributions like Debian 10 Buster

## Adding downstream changes

The standard [Contribution Process](https://wiki.apertis.org/Guidelines/Contribution_process)
applies unchanged to packaging repositories, pushing changes to `wip/` branches
and getting them landed to the `apertis/*` branches via Merge Requests.

The only additional requirement imposed by the Debian packaging format is that
changes outside of the `debian/` folder are not allowed and would cause the
source-building pipeline to fail. Check the [Debian Packaging](https://wiki.debian.org/Packaging)
documentation to find how patches affecting code outside of the `debian/`
folder should be handled.

Updating `debian/changelog` should be done separately as the last step when
issuing a release, generating the changelog entries from the Git commit log,
which makes writing good commit log messages even more important.

A merge request should be submitted on GitLab for each bug or tasks. To ease
the review process, in particular to avoid churn in case or rebases, it is
recommended to leave the editing of `debian/changelog` to a dedicated merge
request once all the other MRs have been landed, see the
[section about landing downstream changes to the main
archive](landing-downstream-changes-to-the-main-archive) below.

If you still wish to edit `debian/changelog` for any reason, just make sure
that the changelog entry you're writing has the `distribution` field set
to `UNRELEASED`, using `gbp dch --auto --ignore-branch` to ensure the
formatting is correct.

The CI pipeline will locally generate a source package for each commit pushed
to the packaging repositories, which can be retrieved by browsing the
pipeline artifacts. The generated sources will be versioned to indicate that
they are not yet suitable for release.

When landing changes to the `apertis/*` branches the CI pipeline will also
upload the built sources to the `:snapshots` OBS project matching the branch
(that is, `apertis:v2020dev0:target:snaphots` when landing changes to the
`apertis/v2020dev0` branch of a `:target` package). A special pre-release
version is forced on the submitted snapshot packages and no Submit Request is
created on OBS until the `tag-release` stage of the CI pipeline is manually
triggered, as described in the [section about landing downstream changes to the
main archive](landing-downstream-changes-to-the-main-archive) below, which also
covers the process of updating `debian/changelog` appropriately.

## Landing downstream changes to the main archive

Once downstream changes to a package are deemed ready to be published in the
Apertis main archive, a proper release needs to be issued.

* Push a `wip/` branch updating `debian/changelog`
  * use `GBP_CONF_FILES=/dev/null gbp dch --release -D apertis --debian-branch=apertis/v2020dev0`
    to generate a release changelog entry summarizing all the changes already
    landed on the `apertis/v2020dev0` branch
  * ensure that the `distribution` field has been changed from `UNRELEASED`
    to `apertis`
* Create a Merge Request based on your `wip/` branch for the most recent
  release branch where you want to land your changes:
  * for instance, if you want to land changes to both the development and
    stable releases, push your `wip/` source branch and create a MR for the
    development one first (for instance, `apertis/v2020dev0`) and then, once
    merged, create a MR for the stable one ((for instance, `apertis/v2019`)
* Get the Merge Request reviewed and landed
* The CI pipeline will then build the source package as usual and upload it to `:snapshots`
* Manually trigger the `tag-release` pipeline step to:
  * add a Git tag for the release version to the repository
  * rebuild the release source package
  * store the release sources in the `pristine-lfs-source` branch
  * upload the release source package
  * create Submit Requests on OBS targetting the main projects (for instance from
    `apertis:v2020dev0:target:snaphots` to `apertis:v2020dev0:target`)
* Get the Submit Request on OBS reviewed and approved

Waiting for the snapshots package to be built on OBS before proceeding with the
`tag-release` step is not mandatory for release commits.  In any case, release
numbers are cheap, so if anything went wrong issue a new MR to release a fix.

The `tag-release` pipeline step can be triggered clicking on the `▶` "play"
button in the graph shown on the pipeline page:

![Graph of a pipeline blocked on manual step](imgs/pipeline-dag-blocked-on-manual-step.png)

The same `▶` button is also directly available from the pipelines listing:

![Pipeline blocked on manual step in the pipeline listing](imgs/pipeline-list-blocked-on-manual-step.png)

When successful, all the pipeline step should turn green and one or more Submit
Requests should be waiting for approval in OBS:

![Successful pipeline graph](imgs/pipeline-dag-successful.png)

When landing the same changes to multiple releases, once the the code
is landed via a MR to a release branch it is possible to simply fast-forward
the other branches without review. Developers need to be careful though, as
it's always better to have a second pair or eyes on where it is appropriate to
land each change.

For trivial changes it is also possible to combine the release commit in the same
MR as the changes. Again, developers need to be careful to ensure the changelog
entries are kept up-to-date when the commit messages get changed via rebase.

## Pulling changes from upstream distributions

A separate set of pipeline steps are configured on the `debian/$RELEASE-gitlab-update-job`
branches (for instance, `debian/buster-gitlab-update-job`) of each package.

The pipeline will check the Debian archive for updates, pull them in the
`debian/$RELEASE` branch (for instance, `debian/buster`), try to merge the new
contents with the matching `apertis/*` branches and, if successful, push a
proposed updates branch while creating a Merge Request for each `apertis/*`
branches it should be landed on.

The upstream update pipeline is scheduled to run automatically each weekend,
but can be manually triggered from the GitLab web UI by selecting the
`Run Pipeline` button in the `Pipelines` page of each repository under `pkg/*`
and selecting the `debian/buster-gitlab-update-job` branch as the reference.

![Run Pipeline button](imgs/run-pipeline-button.png)

Reviewers can then force-push to the proposed update branch in the Merge
Request to fixup any issue caused by the automated merge, and ultimately land
the MRs to the `apertis/*` branches. See the [Landing changes to the main archive][]
section to propagate the updated sources to OBS.

In some situations the automated merge machinery may ask to
`PLEASE SUMMARIZE remaining Apertis Changes`, and in that case
reviewers should:
* check out the proposed update branch
* edit the changelog to list **all** the downstream changes the package still
  ships compared to the newly merged upstream package and their reason,
  describing the purpose of each downstream patch and of any other change
  shown by `git diff` against the `debian/*` branch
* amend the merge commit
* force-push to the proposed update branch
* land the associated Merge Requests as described above
  * land the Merge Request
  * check the snapshot package uploaded to `:snapshots`
  * manually trigger the `tag-release` pipeline step to:
    * add a Git tag for the release version to the repository
    * rebuild the release source package
    * store the release sources in the `pristine-lfs-source` branch
    * upload the release source package
    * create Submit Requests on OBS targetting the main projects (for instance from
      `apertis:v2020dev0:target:snaphots` to `apertis:v2020dev0:target`)
  * get the Submit Request on OBS reviewed and approved

## Diverging release branches

Sometimes different downstream patches need to be applied in the different
Apertis release branches. A clear case of that is the `base-files` package
which ships the release name in `/etc/os-release`.

In such situation it is crucial to use different version identifiers in each
branch: the version for a given package needs to be globally unique across the whole
archive since uploading different package sources with the same name/version
would lead to errors difficult to diagnose.

When targeting a specific release, `~${RELEASE}.${COUNTER}` needs to be
appended to the version identifier after the local build suffix:

* `0.42` → append `co1~v2020pre.0` → `0.42co1~v2020pre.0`
* `0.42co3` → bump to `co4` and append `~v2020pre.0`→ `0.42co4~v2020pre.0`
* `0.42co4~v2020pre.0` → increase the release-specific counter → `0.42co4~v2020pre.1`

This uses the fact that `~` in Debian package numbers sorts before anything,
see the [Debian Policy §5.6.12](https://www.debian.org/doc/debian-policy/ch-controlfields.html#s-f-version) for more details.
Adding `~` is necessary so that if a new upstream version `0.42.1` or a new
non-release-specific downstream version `0.42co4` is introduced, they will
replace the release-specific package.

Note that `dpkg` considers `2020.0` to be newer than `2020pre.0`, so the
Apertis release identifiers can be used with no modification (if in doubt,
check with `dpkg --compare-versions 2020pre.0 '<<' 2020.0 && echo ok`).

## Adding new packages from Debian

This is the process to import a new package from Debian to Apertis:

* locally create a new git repository
* invoke `import-debian-package` from the [packaging-tools
  repository](https://gitlab.apertis.org/infrastructure/packaging-tools/)
  to populate the local git repository:
  * fetch a specific version: `import-debian-package --upstream buster --downstream apertis/v2020dev0 --create-ci-branches --package hello --version 2.10-2`
  * fetch the latest version: `import-debian-package --upstream buster --downstream apertis/v2020dev0 --create-ci-branches --package hello`
  * multiple downstream branches can be specified, in which case all of them
    will be updated to point to the newly imported package version
  * don't use `import-debian-package` on existing repositories, it does not
    attempt to merge `apertis/*` branches and instead it re-sets them to new
    branches based on the freshly imported Debian package
* create an empty project on GitLab under the `pkg/*` namespaces (for instance, `pkg/target/hello`)
* set it up with `gitlab-rulez apply rulez.yaml --filter $REPOSITORYPATH` from
  the [gitlab-rulez repository](https://gitlab.apertis.org/infrastructure/gitlab-rulez)
  * sets the CI config path to `debian/apertis/gitlab-ci.yml`
  * changes the merge request settings:
      * only allow fast-forward merges
      * ensure merges are only allowed if pipelines succeed
  * adds a schedule on the `debian/buster-gitlab-update-job` branch to run weekly
  * marks the `apertis/*` and `debian/*` branches as protected
* configure the origin remote on your local git: `git remote add origin git@gitlab.apertis.org:pkg/target/hello`
* push your local git contents to the newly created GitLab project: `git push --all --follow-tags origin`
* follow the process described in the [section about landing downstream changes
  to the main archive](landing-downstream-changes-to-the-main-archive) above to
  publish the package on OBS.

# Internals

Main components:
* [`ci-package-builder`](https://gitlab.apertis.org/infrastructure/ci-package-builder):
  centralized location of the GitLab-to-OBS and Debian-to-GitLab pipeline definitions
* [`debian/apertis/gitlab-ci.yaml`](https://gitlab.apertis.org/pkg/target/base-files/blob/apertis/v2019/debian/apertis/gitlab-ci.yml):
  imports the `ci-package-builder` pipelines from each packaging repository
* [`apertis-package-source-builder`](https://gitlab.apertis.org/infrastructure/apertis-docker-images/tree/apertis/v2019/apertis-package-source-builder):
  Docker environment for the GitLab pipelines
* [`pristine-lfs`](https://salsa.debian.org/andrewsh/pristine-lfs): stores
  upstream original tarballs and packaging source tarballs using Git-LFS, as a
  more robust replacement for `pristine-tar`

Branches:
* `pristine-lfs`: stores references to the Git-LFS-hosted original tarballs
* `debian/$DEBIAN_RELEASE` (for instance, `debian/buster`): contains the extracted
  upstream sources and packaging information from Debian
* `pristine-lfs-source`: stores references to the Git-LFS-hosted packaging
  tarballs, mainly to ensure that each (package, version) tuple is built only
  once and no conflicts can arise
* `apertis/$APERTIS_RELEASE` (for intance, `apertis/v2020dev0`): contains the
  extracted upstream sources and possibly patched packaging information for
  Apertis, including the `debian/apertis/gitlab-ci.yaml` to set up the
  GitLab-to-OBS pipeline
* `debian/$DEBIAN_RELEASE-gitlab-update-job` (for instance,
  `debian/buster-gitlab-update-job`): hosts the `debian/apertis/gitlab-ci.yaml`
  file to configure the Debian-to-GitLab pipeline

Tags:
* `debian/*`: tags for Debian releases in the `debian/*` branches
* `apertis/*`:  tags for the Apertis releases in the `apertis/*` branches
